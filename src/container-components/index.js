import loginPage from './login.page'
import registerPage from './register.page'
import dashboardPage from './dashboard.page'

export {
  loginPage,
  registerPage,
  dashboardPage
}
