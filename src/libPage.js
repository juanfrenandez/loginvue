import * as containerComponents from './container-components'

/* eslint-disable no-var, no-undef, guard-for-in, object-shorthand */

const VuePlugin = {
  install: function (Vue) {
    if (Vue._login_vue_installed) {
      return
    }

    Vue._login_vue_installed = true

    // Register components
    for (var containerComponent in containerComponents) {
      console.log('registrando componente', containerComponent)
      Vue.component(containerComponent, containerComponents[containerComponent])
    }
  }
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VuePlugin)
}

export default VuePlugin
