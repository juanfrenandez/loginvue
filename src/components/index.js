import LoginComponent from './login.component'
import login2 from './login2.component'
import register from './register.component'
import cuentaUser from './cuentaUser.component'
import menuDashbaordUser from './menuDashbaordUser.component'
import headerVue from './header.component'

export {
  LoginComponent,
  login2,
  register,
  cuentaUser,
  menuDashbaordUser,
  headerVue
}
