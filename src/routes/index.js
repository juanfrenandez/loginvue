import * as containerComponents from '../container-components'

const layout = {
  template: `
    <div class="layout">
      <h2>Layout</h2>
      <router-view></router-view>
    </div>
  `
}

export default [{
  path: '/l',
  component: layout,
  children:
  [
    {
      path: '',
      name: 'login',
      component: containerComponents.loginPage
    },
    {
      path: 'registrar',
      name: 'registrar',
      component: containerComponents.registerPage
    },
    {
      path: 'dashboard',
      name: 'dashboard',
      component: containerComponents.dashboardPage
    }
  ]

}

]
