import * as components from './components'
import * as containerComponents from './container-components'
import routes from './routes'
/* eslint-disable no-var, no-undef, guard-for-in, object-shorthand */

const VuePlugin = {
  routes,
  // defaultComponent: containerComponents.dashboardpage,
  install: function (Vue) {
    if (Vue._login_vue_installed) {
      return
    }

    Vue._login_vue_installed = true

    // Register components
    for (var component in components) {
      Vue.component(component, components[component])
    }

    // Register container components
    for (var containerComponent in containerComponents) {
      Vue.component(containerComponent, containerComponents[containerComponent])
    }
  }
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VuePlugin)
}

export default VuePlugin
