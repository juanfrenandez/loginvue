// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import loginVue from './lib'
import App from './app'

Vue.use(loginVue)
Vue.config.productionTip = false

import Router from 'vue-router'
Vue.use(Router)
const router = new Router({
  routes: [...loginVue.routes]
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
